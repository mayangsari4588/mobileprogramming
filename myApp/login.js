import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Logo from './logo';
import Form from './form';
export default class login extends Component {
    render() {
        return(
            <View style = {StyleSheet.container}>
                <Logo />
                <Form />
                <View style = {styles.signupTextCont}>
                <Text style={styles.signupText}>Don’t have an Account? </Text>
                <Text style={styles.signupButton}>Sign Up</Text> 
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
          flex: 1,
          backgroundColor: 'black',
          alignItems: 'center',
          justifyContent: 'center'
        },
    signupTextCont: {
            flexGrow: 1,
            alignItems: 'flex-end',
            justifyContent: 'center',
            marginVertical: 16,
            flexDirection: 'row'
        },
    signupText: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16
    },
    signupButton: {
        color: '#0099FF',
        fontSize: 16,
        fontWeight: '500'
    }
});