import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/mayangsari.jpg')} />
                    </View>
                    <Text style={styles.name}>Mayangsari</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>1</Text>
                        <Text style={styles.statTitle}>Post</Text>
                    </View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program study : </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>kelas : </Text>
                    <Text style ={styles.state}>Pagi A</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram :</Text>
                    <Text style ={styles.state}>mygs23</Text>
                </View>
                <View style={styles.twitter}>
                    <Text style={styles.twt}>Twitter :</Text>
                    <Text style ={styles.state}>mygs23_</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'black',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: 'white',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'white',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'white',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 20,
        left: 60
    },
    study:{
        justifyContent: 'flex-end',
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
    },
    class: {
        bottom:-20,
        color: 'white',
        left: 60
    },
    kelas:{
        fontWeight: 'bold',
        color: 'white',
        fontSize : 30
    },
    instagram: {
        bottom : -50,
        color: 'white',
        left : 60
    },
    ig: {
        fontSize: 30,
        color: 'white',
        fontWeight: 'bold'
    },
    twitter:{
        bottom: -90,
        color: 'white',
        left : 60
    },
    twt: {
        fontWeight :'bold',
        color: 'white',
        fontSize: 30
    },
    state : {
        fontSize : 25,
        color: 'white',
        left : 60,
        bottom :-15
    }
});